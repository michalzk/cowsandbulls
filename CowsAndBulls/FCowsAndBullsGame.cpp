#include "FCowsAndBullsGame.h"
#include <iostream>
#include <map>
#define TMap std::map

using int32 = int; 
using FString = std::string;

int32 FCowsAndBullsGame::getCurrentTry() const { return currentTry; }
bool FCowsAndBullsGame::isGameWon() const {	return bGameWon; }
int32 FCowsAndBullsGame::getCorrectWordLength() const {	return correctWord.length(); }

FCowsAndBullsGame::FCowsAndBullsGame() { reset(); }

int32 FCowsAndBullsGame::getMaxTries() const 
{ 
	TMap<int32, int32> mapWordLengthToMaxTries = { { 3,5 },{ 4,7 },{ 5,10 },{ 6,16 },{ 7,20 }  };

	return mapWordLengthToMaxTries[correctWord.length()];
}

void FCowsAndBullsGame::reset()
{
	currentTry = 1;
	const FString CORRECT_WORD = "planet";
	correctWord = CORRECT_WORD;
	bGameWon = false;
	return;
}

EWordValidity FCowsAndBullsGame::checkGuessValidity(FString guess) const
{
	if (!IsIsogram(guess)) {
		return EWordValidity::not_isogram;
	}
	else if (!IsLowercase(guess)) {
		return EWordValidity::not_lowercase;
	}
	else if (guess.length() != getCorrectWordLength() ) {
		return EWordValidity::wrong_length;
	}
	else {
		return EWordValidity::ok;
	}
}

FBullCowCount FCowsAndBullsGame::SubmitGuess(FString guess)
{
	currentTry++;
	FBullCowCount BullCowCount;
	int32 correctWordLength = getCorrectWordLength();
	for (int32 hWordChar = 0; hWordChar < correctWordLength; hWordChar++) {
		for (int32 gWordChar = 0; gWordChar < correctWordLength; gWordChar++) {
			if (guess[gWordChar] == correctWord[hWordChar]) {
				if (hWordChar == gWordChar) {
					BullCowCount.Bulls++;
				}
				else {
					BullCowCount.Cows++;
				}
			}
 		}
	}
	if (BullCowCount.Bulls == correctWordLength) {
		bGameWon = true;
	}
	else {
		bGameWon = false;
	}
	return BullCowCount;
}

bool FCowsAndBullsGame::IsIsogram(FString word) const
{
	if (word.length() <= 1) { return true; }

	TMap<char, bool> letterSeen;

	for (auto letter : word) {
		letter = tolower(letter);
		if (letterSeen[letter]) {
			return false;
		}
		else {
			letterSeen[letter] = true;
		}
	}

}

bool FCowsAndBullsGame::IsLowercase(FString word) const
{
	for (auto letter : word) {
		if (!islower(letter)) {
			return false;
		}
	}
	return true;
}
