#pragma once
#include <string>

using FString = std::string;
using int32 = int;

// we write initializing struct for bulls and cows
struct FBullCowCount {
	int32 Bulls = 0;
	int32 Cows = 0;
};

enum class EWordValidity {
	ok,
	not_isogram,
	wrong_length,
	not_lowercase,
	invalid_status
};

//game class
class FCowsAndBullsGame {
public:
	FCowsAndBullsGame(); //constructor
	void reset();
	int32 getMaxTries() const;
	int32 getCurrentTry() const;
	int32 getCorrectWordLength() const;
	bool isGameWon() const;
	EWordValidity checkGuessValidity(FString guess) const;
	FBullCowCount SubmitGuess(FString guess); //we provide counter for valid guesses
private:
	int32 currentTry;
 	int32 maxTries;
	FString correctWord;
	bool bGameWon;

	bool IsIsogram(FString) const;
	bool IsLowercase(FString) const;
};
