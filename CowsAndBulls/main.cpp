#pragma once

#include <iostream>
#include <string>
#include "FCowsAndBullsGame.h"

using FText = std::string;
using int32 = int;

//function definitions
void printWelcomeMessage(int n);
void playGame();
FText enterValidGuess();
bool askToPlayAgain();
void finishGameMessage();

FCowsAndBullsGame BCGame;

int main()
{
	bool bQuit = false;
	do {
		printWelcomeMessage(BCGame.getCorrectWordLength());
		playGame();
		bQuit = !askToPlayAgain();
	} while (bQuit != true);
	return 0;
}

void printWelcomeMessage(int n) {
	std::cout << "\nWelcome to the cow and bulls game!\n";
	std::cout << "Can you guess the " << n << " letters isogram I am thinking of?\n";
}

void playGame() {
	BCGame.reset();
	int32 turns = BCGame.getMaxTries();
	while(!BCGame.isGameWon() && BCGame.getCurrentTry() <= turns) {
		FText guess = enterValidGuess();
		FBullCowCount BullCowCount = BCGame.SubmitGuess(guess);
		std::cout << "Bulls: " << BullCowCount.Bulls << ". Cows: " << BullCowCount.Cows << "." << std::endl;
	}
	finishGameMessage();
}

FText enterValidGuess() {
	EWordValidity guessStatus = EWordValidity::invalid_status;
	FText guess = "";
	do {
		int32 currentTry = BCGame.getCurrentTry();
		std::cout << "\nTry " << currentTry << ". Enter your guess: ";
		std::getline(std::cin, guess);
		guessStatus = BCGame.checkGuessValidity(guess);
		switch (guessStatus) {
		case EWordValidity::not_isogram:
			std::cout << "The letters in word " << guess << " are repeating.\n";
			break;
		case EWordValidity::not_lowercase:
			std::cout << "The letters in word " << guess << " are not lowercase.\n";
			break;
		case EWordValidity::wrong_length:
			std::cout << "Please enter a word that has " << BCGame.getCorrectWordLength() << " letters.\n";
			break;
		default:
			return guess;
			break;
		}
	} while (guessStatus != EWordValidity::ok);
	return guess;
}

bool askToPlayAgain() {
	std::cout << "\nDo you want to play again? (y/n) ";
	FText decision;
	std::getline(std::cin, decision);
	return (decision[0] == 'Y' || decision[0] == 'y');
}

void finishGameMessage()
{
	if (BCGame.isGameWon()) {
		std::cout << "Well done - You won the game!\n";
	}
	else {
		std::cout << "You lost - try next time!\n";
	}
}
